package ru.tsc.fuksina.tm.repository;

import ru.tsc.fuksina.tm.api.ICommandRepository;
import ru.tsc.fuksina.tm.constant.ArgumentConst;
import ru.tsc.fuksina.tm.constant.TerminalConst;
import ru.tsc.fuksina.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info"
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display application version"
    );

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands"
    );

    private static final Command INFO = new Command (
            TerminalConst.INFO, ArgumentConst.INFO, "Show system info"
    );

    private static final Command ARGUMENT = new Command(
            TerminalConst.ARGUMENT, ArgumentConst.ARGUMENT, "Show application arguments"
    );

    private static final Command COMMAND = new Command(
            TerminalConst.COMMAND, ArgumentConst.COMMAND, "Show application commands"
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close application"
    );

    private final Command[] terminalCommands = new Command[] {
            ABOUT,
            VERSION,
            HELP,
            INFO,
            ARGUMENT,
            COMMAND,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }
}
