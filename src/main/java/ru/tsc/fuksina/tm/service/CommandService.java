package ru.tsc.fuksina.tm.service;

import ru.tsc.fuksina.tm.api.ICommandRepository;
import ru.tsc.fuksina.tm.api.ICommandService;
import ru.tsc.fuksina.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
