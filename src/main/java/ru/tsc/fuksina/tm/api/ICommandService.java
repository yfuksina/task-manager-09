package ru.tsc.fuksina.tm.api;

import ru.tsc.fuksina.tm.model.Command;

public interface ICommandService {
    Command[] getTerminalCommands();
}
